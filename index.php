<?php

include_once($_SERVER['DOCUMENT_ROOT']."/Pitagoras/controller/NewsletterSubscriptionController.php");
include_once($_SERVER['DOCUMENT_ROOT']."/Pitagoras/scripts/logout.php");

if(isset($_GET['subscribe']))
{
	$subscription = new NewsletterSubscriptionController($_POST);
	$subscription->new();
	setcookie("subscribed", 1);
	Header("Location: index.php");
}

?>

<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Autoescola Pitágoras. Venha conhecer nossa instituição localizada no bairro Velha em Blumenau.">

    <link rel="stylesheet" href="bootstrap/bootstrap.css">
    <link rel="stylesheet" href="styles.css">

    <link rel="sortcut icon" href="assets/logo.png" type="image/png"/>

    <!-- AOS Library -->
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

    <title>CFC Pitágoras</title>
  </head>
  <body onresize="setBreakpoint()">
		<div class="modal fade" tabindex="-1" role="dialog" id="newsletter-modal">
		  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		    <div class="modal-content">
		      <div class="modal-body text-center d-flex flex-column align-items-center">
		      	<h3 class="modal-title d-flex align-items-center">
							<svg class="bi bi-newspaper mr-2" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
							  <path fill-rule="evenodd" d="M0 2A1.5 1.5 0 011.5.5h11A1.5 1.5 0 0114 2v12a1.5 1.5 0 01-1.5 1.5h-11A1.5 1.5 0 010 14V2zm1.5-.5A.5.5 0 001 2v12a.5.5 0 00.5.5h11a.5.5 0 00.5-.5V2a.5.5 0 00-.5-.5h-11z" clip-rule="evenodd"/>
							  <path fill-rule="evenodd" d="M15.5 3a.5.5 0 01.5.5V14a1.5 1.5 0 01-1.5 1.5h-3v-1h3a.5.5 0 00.5-.5V3.5a.5.5 0 01.5-.5z" clip-rule="evenodd"/>
							  <path d="M2 3h10v2H2V3zm0 3h4v3H2V6zm0 4h4v1H2v-1zm0 2h4v1H2v-1zm5-6h2v1H7V6zm3 0h2v1h-2V6zM7 8h2v1H7V8zm3 0h2v1h-2V8zm-3 2h2v1H7v-1zm3 0h2v1h-2v-1zm-3 2h2v1H7v-1zm3 0h2v1h-2v-1z"/>
							</svg>
							Newsletter
						</h3>
		        <p class="card-text text-muted h5">Assine e esteja sempre em dia sobre nossas novidades!</p>
		        <form method="post" action="?subscribe=1" class="text-center w-100 mt-3 px-3">
							<div class="form-group w-100">
								<label for="name" class="h6 text-dark">Nome</label>
								<input type="text" class="form-control text-center" id="name" name="name" maxlength="100" required>
							</div>
							<div class="form-group w-100">
								<label for="email" class="h6 text-dark">Email</label>
								<input type="email" class="form-control text-center" id="email" name="email" maxlength="70" required>
							</div>
							<div class="form-group w-100">
								<label for="phone" class="h6 text-dark">Celular</label>
								<input type="text" class="form-control text-center" id="phone" name="phone" maxlength="14">
							</div>
							<button id="send" class="btn btn-dark font-weight-bold mt-2 px-4 w-100">Enviar</button>
							<button class="btn btn-link text-decoration-none text-muted h6 mt-2" data-dismiss="modal">Talvez mais tarde.</button>
						</form>
					</div>
		    </div>
		  </div>
		</div>

		<button class="btn btn-dark shadow rounded-circle fixed-bottom m-3 d-sm-none left-auto" data-toggle="modal" data-target="#menuModal" id="menuToggler">
			<svg class="bi bi-list" width="2em" height="2em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
			  <path fill-rule="evenodd" d="M2.5 11.5A.5.5 0 013 11h10a.5.5 0 010 1H3a.5.5 0 01-.5-.5zm0-4A.5.5 0 013 7h10a.5.5 0 010 1H3a.5.5 0 01-.5-.5zm0-4A.5.5 0 013 3h10a.5.5 0 010 1H3a.5.5 0 01-.5-.5z" clip-rule="evenodd"/>
			</svg>
		</button>
		<div class="modal fade" id="menuModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog modal-dialog-centered" role="document">
		    <div class="modal-content">
		      <div class="modal-header justify-content-center">
		        <h5 class="modal-title" id="exampleModalLabel">Menu</h5>
		      </div>
		      <div class="modal-body d-flex flex-column align-items-center">
			      <a class="btn btn-link text-dark text-decoration-none d-flex align-items-center" href="index.php">
			      	<svg class="bi bi-house mr-1" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
							  <path fill-rule="evenodd" d="M2 13.5V7h1v6.5a.5.5 0 00.5.5h9a.5.5 0 00.5-.5V7h1v6.5a1.5 1.5 0 01-1.5 1.5h-9A1.5 1.5 0 012 13.5zm11-11V6l-2-2V2.5a.5.5 0 01.5-.5h1a.5.5 0 01.5.5z" clip-rule="evenodd"/>
							  <path fill-rule="evenodd" d="M7.293 1.5a1 1 0 011.414 0l6.647 6.646a.5.5 0 01-.708.708L8 2.207 1.354 8.854a.5.5 0 11-.708-.708L7.293 1.5z" clip-rule="evenodd"/>
							</svg>
			      	Home
			     	</a>
			      <a class="btn btn-link text-dark text-decoration-none d-flex align-items-center" href="view/shop.php">
			      	<svg class="bi bi-bag mr-1" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
							  <path fill-rule="evenodd" d="M14 5H2v9a1 1 0 001 1h10a1 1 0 001-1V5zM1 4v10a2 2 0 002 2h10a2 2 0 002-2V4H1z" clip-rule="evenodd"/>
							  <path d="M8 1.5A2.5 2.5 0 005.5 4h-1a3.5 3.5 0 117 0h-1A2.5 2.5 0 008 1.5z"/>
							</svg>
			      	Ofertas
			      </a>
			      <a class="btn btn-link text-dark text-decoration-none d-flex align-items-center" href="view/news.php">
			      	<svg class="bi bi-file-post mr-1" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
							  <path fill-rule="evenodd" d="M4 1h8a2 2 0 012 2v10a2 2 0 01-2 2H4a2 2 0 01-2-2V3a2 2 0 012-2zm0 1a1 1 0 00-1 1v10a1 1 0 001 1h8a1 1 0 001-1V3a1 1 0 00-1-1H4z" clip-rule="evenodd"/>
							  <path d="M4 5.5a.5.5 0 01.5-.5h7a.5.5 0 01.5.5v7a.5.5 0 01-.5.5h-7a.5.5 0 01-.5-.5v-7z"/>
							  <path fill-rule="evenodd" d="M4 3.5a.5.5 0 01.5-.5h5a.5.5 0 010 1h-5a.5.5 0 01-.5-.5z" clip-rule="evenodd"/>
							</svg>
			      	Novidades</a>
			      <a class="btn btn-link text-dark text-decoration-none d-flex align-items-center" href="view/contact.php">
			      	<svg class="bi bi-chat-square mr-1" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
							  <path fill-rule="evenodd" d="M14 1H2a1 1 0 00-1 1v8a1 1 0 001 1h2.5a2 2 0 011.6.8L8 14.333 9.9 11.8a2 2 0 011.6-.8H14a1 1 0 001-1V2a1 1 0 00-1-1zM2 0a2 2 0 00-2 2v8a2 2 0 002 2h2.5a1 1 0 01.8.4l1.9 2.533a1 1 0 001.6 0l1.9-2.533a1 1 0 01.8-.4H14a2 2 0 002-2V2a2 2 0 00-2-2H2z" clip-rule="evenodd"/>
							</svg>
			      	Contato
			      </a>
		      </div>
		    </div>
		  </div>
		</div>

		<nav id="menu" class="navbar px-4 py-3 navbar-expand-sm navbar-light w-100 d-flex bg-yellow justify-content-end justify-content-sm-between">
			<div class="collapse navbar-collapse" id="navbarNav">
			  <ul class="navbar-nav">
			  	<li class="nav-item">
			      <a class="nav-link" href="index.php">Home</a>
			    </li>
			    <li class="nav-item">
			      <a class="nav-link" href="view/shop.php">Ofertas</a>
			    </li>
			    <li class="nav-item">
			      <a class="nav-link" href="view/news.php">Novidades</a>
			    </li>
			    <li class="nav-item">
			      <a class="nav-link" href="view/contact.php">Contato</a>
			    </li>
			  </ul>
			</div>
			<?php

		  if (isset($_SESSION['logged'])) {
	   	
	   	?>
	    	<span class='nav-item dropdown'>
	        <a class='nav-link dropdown-toggle text-dark h6' 
	        	href='#' 
	        	id='navbarDropdown' 
	        	role='button' 
	        	data-toggle='dropdown' 
	        	aria-haspopup='true' 
	        	aria-expanded='false'>
	          <?php echo $_SESSION['buyerName']; ?>
	        </a>
	        <div class='dropdown-menu dropdown-menu-right' aria-labelledby='navbarDropdown'>
	          <a class='dropdown-item d-flex align-items-center' href='view/profile.php'>
	          	<svg class='bi bi-people-circle mr-1' width='1em' height='1em' viewBox='0 0 16 16' fill='currentColor' xmlns='http://www.w3.org/2000/svg'>
							  <path d='M13.468 12.37C12.758 11.226 11.195 10 8 10s-4.757 1.225-5.468 2.37A6.987 6.987 0 008 15a6.987 6.987 0 005.468-2.63z'/>
							  <path fill-rule='evenodd' d='M8 9a3 3 0 100-6 3 3 0 000 6z' clip-rule='evenodd'/>
							  <path fill-rule='evenodd' d='M8 1a7 7 0 100 14A7 7 0 008 1zM0 8a8 8 0 1116 0A8 8 0 010 8z' clip-rule='evenodd'/>
							</svg>
	          	Meu perfil
	          </a>
	          <div class='dropdown-divider'></div>
	          <a class='dropdown-item text-danger d-flex align-items-center' href='?logout=1'>
	          	<svg class='bi bi-box-arrow-in-left mr-1' width='1em' height='1em' viewBox='0 0 16 16' fill='currentColor' xmlns='http://www.w3.org/2000/svg'>
							  <path fill-rule='evenodd' d='M7.854 11.354a.5.5 0 000-.708L5.207 8l2.647-2.646a.5.5 0 10-.708-.708l-3 3a.5.5 0 000 .708l3 3a.5.5 0 00.708 0z' clip-rule='evenodd'/>
							  <path fill-rule='evenodd' d='M15 8a.5.5 0 00-.5-.5h-9a.5.5 0 000 1h9A.5.5 0 0015 8z' clip-rule='evenodd'/>
							  <path fill-rule='evenodd' d='M2.5 14.5A1.5 1.5 0 011 13V3a1.5 1.5 0 011.5-1.5h8A1.5 1.5 0 0112 3v1.5a.5.5 0 01-1 0V3a.5.5 0 00-.5-.5h-8A.5.5 0 002 3v10a.5.5 0 00.5.5h8a.5.5 0 00.5-.5v-1.5a.5.5 0 011 0V13a1.5 1.5 0 01-1.5 1.5h-8z' clip-rule='evenodd'/>
							</svg>
	          	Sair
	          </a>
	        </div>
	      </span>
	    
	    <?php

	    }

		  ?>
		</nav>

		<main class="d-flex flex-column w-100 text-center shadow-lg bg-yellow mh-85vh br-radius bl-radius">
			<div class="w-100 pb-5 d-flex flex-column justify-content-center mh-85vh">
				<div class="d-flex flex-column flex-wrap flex-sm-row justify-content-around align-items-center py-3 px-5 w-100">
					<div class="col-lg-5 text-lg-left p-2">
						<h1 class="title-width display-4 font-weight-bold d-none d-lg-block">CFC Pitágoras</h1>
						<h1 class="title-width font-weight-bold d-lg-none">CFC Pitágoras</h1>
						<h2 class="font-weight-normal">O Centro de Formação de Condutores que irá transformar sua visão no trânsito.</h2>
						<a href="#hello" class="btn btn-outline-dark btn-lg mt-3">Quero saber mais!</a>
					</div>
					<div class="col-lg-5 p-2">
						<img src="assets/driver.svg" alt="Ilustração" class="mw-100">
					</div>
				</div>
			</div>
		</main>

		<section class="container fluid d-flex flex-column text-center align-items-center py-5 px-4" id="hello">
			<div data-aos="fade-up">	
				<h1 class="font-weight-bold">Olá, condutor!</h1>
				<h4 class="text-muted">Novo por aqui? Navegue por nosso site e conheça nossos serviços!</h4>
			</div>
			<div class="d-flex flex-column justify-content-center w-100 my-3">
				<div class="row justify-content-center">
					<div class="card m-2 w-100 col-sm-10 shadow card-max" data-aos="fade-up">
					  <div class="card-body">
					    <h5 class="card-title d-flex align-items-center justify-content-center">
					    	<svg class="bi bi-bag mr-1" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
								  <path fill-rule="evenodd" d="M14 5H2v9a1 1 0 001 1h10a1 1 0 001-1V5zM1 4v10a2 2 0 002 2h10a2 2 0 002-2V4H1z" clip-rule="evenodd"/>
								  <path d="M8 1.5A2.5 2.5 0 005.5 4h-1a3.5 3.5 0 117 0h-1A2.5 2.5 0 008 1.5z"/>
								</svg>
					    	Ofertas
					   	</h5>
					    <p class="card-text">Confira nossos serviços disponíveis e contrate-os agora mesmo.</p>
					  </div>
					  <div class="card-footer bg-white">
					  	<a href="view/shop.php" class="btn btn-link text-decoration-none font-yellow">Saber mais</a>
					  </div>
					</div>
					<div class="card m-2 w-100 col-sm-10 shadow card-max" data-aos="fade-up">
					  <div class="card-body">
					    <h5 class="card-title d-flex justify-content-center align-items-center">
					    	<svg class="bi bi-file-post mr-1" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
								  <path fill-rule="evenodd" d="M4 1h8a2 2 0 012 2v10a2 2 0 01-2 2H4a2 2 0 01-2-2V3a2 2 0 012-2zm0 1a1 1 0 00-1 1v10a1 1 0 001 1h8a1 1 0 001-1V3a1 1 0 00-1-1H4z" clip-rule="evenodd"/>
								  <path d="M4 5.5a.5.5 0 01.5-.5h7a.5.5 0 01.5.5v7a.5.5 0 01-.5.5h-7a.5.5 0 01-.5-.5v-7z"/>
								  <path fill-rule="evenodd" d="M4 3.5a.5.5 0 01.5-.5h5a.5.5 0 010 1h-5a.5.5 0 01-.5-.5z" clip-rule="evenodd"/>
								</svg>
					    	Novidades
					    </h5>
					    <p class="card-text">Fique por dentro do que está acontecendo no nosso centro de formação e no trânsito.</p>
					  </div>
					  <div class="card-footer bg-white">
					  	<a href="view/news.php" class="btn btn-link text-decoration-none font-yellow">Saber mais</a>
					  </div>
					</div>
				</div>
				<div class="row justify-content-center d-flex">
					<div class="card m-2 w-100 col-sm-10 shadow card-max" data-aos="fade-up">
					  <div class="card-body">
					    <h5 class="card-title d-flex justify-content-center align-items-center">
					    	<svg class="bi bi-info-circle mr-1" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
								  <path fill-rule="evenodd" d="M8 15A7 7 0 108 1a7 7 0 000 14zm0 1A8 8 0 108 0a8 8 0 000 16z" clip-rule="evenodd"/>
								  <path d="M8.93 6.588l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588z"/>
								  <circle cx="8" cy="4.5" r="1"/>
								</svg>
					    	Sobre Nós
					    </h5>
					    <p class="card-text">Um pouco da nossa história e nossa instituição.</p>
					  </div>
					  <div class="card-footer bg-white">
					  	<a href="#about" class="btn btn-link text-decoration-none font-yellow">Saber mais</a>
					  </div>
					</div>
					<div class="card m-2 w-100 col-sm-10 shadow card-max" data-aos="fade-up">
					  <div class="card-body">
					    <h5 class="card-title d-flex justify-content-center align-items-center">
					    	<svg class="bi bi-chat-square mr-1" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
								  <path fill-rule="evenodd" d="M14 1H2a1 1 0 00-1 1v8a1 1 0 001 1h2.5a2 2 0 011.6.8L8 14.333 9.9 11.8a2 2 0 011.6-.8H14a1 1 0 001-1V2a1 1 0 00-1-1zM2 0a2 2 0 00-2 2v8a2 2 0 002 2h2.5a1 1 0 01.8.4l1.9 2.533a1 1 0 001.6 0l1.9-2.533a1 1 0 01.8-.4H14a2 2 0 002-2V2a2 2 0 00-2-2H2z" clip-rule="evenodd"/>
								</svg>
					    	Contato
					    </h5>
					    <p class="card-text">Deixe-nos sua mensagem ou entre em contato por um dos nossos canais de comunicação.</p>
					  </div>
					  <div class="card-footer bg-white">
					  	<a href="view/contact.php" class="btn btn-link text-decoration-none font-yellow">Saber mais</a>
					  </div>
					</div>
				</div>
			</div>
		</section>

		<section class="bg-yellow px-4 py-5 my-3 tr-radius br-radius shadow" data-aos="fade-right" style="max-width: 90%" id="about">
			<h1 class="font-weight-bold">A Instituição</h1>
			<h4 class="mb-3">Conheça um pouco mais sobre nós...</h4>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu porta enim. Pellentesque fermentum purus a lacus dignissim pretium. In hac habitasse platea dictumst. Aenean orci turpis, fringilla a dapibus a, sollicitudin vitae nulla. Integer nec consequat felis, a fringilla dolor. In consequat turpis vel dolor ultrices iaculis. Morbi ut nulla felis. Duis varius cursus erat sit amet eleifend. Duis interdum pharetra mattis. Suspendisse lacinia ultricies diam vel hendrerit. Praesent pretium dictum justo, non placerat nibh posuere eleifend.</p>
			<p>Nulla facilisi. Nullam ultricies convallis sagittis. Sed gravida erat et quam posuere interdum id sollicitudin nibh. Etiam bibendum volutpat dui id laoreet. Vivamus vulputate posuere nunc, a efficitur enim aliquet eget. Fusce quis enim at felis eleifend condimentum eu nec est. Cras vehicula diam risus. Quisque posuere felis a est fringilla, blandit finibus lectus porttitor. Aliquam laoreet justo ut ornare placerat. Mauris eu maximus ante. Ut posuere est lectus, vitae fringilla quam lobortis dignissim. Duis vel elementum odio. Donec at eros a ligula faucibus lobortis.</p>
		</section>

		<section class="d-flex flex-wrap justify-content-center pt-4 pb-2 px-4">
			<div class="card m-3 py-3 w-100 col-sm-6 shadow text-center card-max" data-aos="zoom-in">
			  <div class="card-body">
			    <h3 class="card-title d-flex justify-content-center align-items-center">
			    	<svg class="bi bi-geo-alt mr-1" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
						<path fill-rule="evenodd" d="M8 16s6-5.686 6-10A6 6 0 002 6c0 4.314 6 10 6 10zm0-7a3 3 0 100-6 3 3 0 000 6z" clip-rule="evenodd"/>
					</svg>
			    	Localização
			    </h3>
			    <h5 class="card-subtitle mb-2 text-muted">Onde nos encontrar?</h5>
			    <p class="card-text">Rua General Osório, 3735 </br> Água Verde, Blumenau - SC</p>
			    <a href="https://goo.gl/maps/pG9DMfrmQGfnAzn36" target="_blank" rel="noopener" class="btn btn-link text-decoration-none text-muted">Ir para o mapa</a>
			  </div>
			</div>
			<div class="card m-3 py-3 w-100 col-sm-6 shadow text-center card-max" data-aos="zoom-in">
			  <div class="card-body">
			    <h3 class="card-title d-flex justify-content-center align-items-center">
			    	<svg class="bi bi-at mr-1" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
						  <path fill-rule="evenodd" d="M13.106 7.222c0-2.967-2.249-5.032-5.482-5.032-3.35 0-5.646 2.318-5.646 5.702 0 3.493 2.235 5.708 5.762 5.708.862 0 1.689-.123 2.304-.335v-.862c-.43.199-1.354.328-2.29.328-2.926 0-4.813-1.88-4.813-4.798 0-2.844 1.921-4.881 4.594-4.881 2.735 0 4.608 1.688 4.608 4.156 0 1.682-.554 2.769-1.416 2.769-.492 0-.772-.28-.772-.76V5.206H8.923v.834h-.11c-.266-.595-.881-.964-1.6-.964-1.4 0-2.378 1.162-2.378 2.823 0 1.737.957 2.906 2.379 2.906.8 0 1.415-.39 1.709-1.087h.11c.081.67.703 1.148 1.503 1.148 1.572 0 2.57-1.415 2.57-3.643zm-7.177.704c0-1.197.54-1.907 1.456-1.907.93 0 1.524.738 1.524 1.907S8.308 9.84 7.371 9.84c-.895 0-1.442-.725-1.442-1.914z" clip-rule="evenodd"/>
						</svg>
			    	Mídias
			    </h3>
			    <h5 class="card-subtitle mb-2 text-muted">Nos acompanhe nas redes sociais.</h5>
			    <p class="card-text">
			    	<a class="link text-decoration-none facebook-color" href="https://www.facebook.com/AutoescolaPitagoras/?ref=settings" target="_blank" rel="noopener"><strong>Facebook &#8599;</strong></a>
			    	</br>
			    	<a class="link text-decoration-none instagram-color" href="https://www.instagram.com/autoescola_pitagoras/" target="_blank" rel="noopener"><strong>Instagram &#8599;</strong></a>
			    </p>
			  </div>
			</div>
		</section>

		<footer class="d-flex justify-content-center py-4 px-4">
			<span class="font-weight-bold text-dark">
				Desenvolvido por
				<a class="link text-decoration-none instagram-color" href="https://www.instagram.com/scherer_programmer/" target="_blank" rel="noopener">
					<strong>Ruan Scherer &#8599;</strong>
				</a>
			</span>
		</footer>

    <script src="js/jquery.slim.min.js"></script>
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/menu.js"></script>
    <script src="js/masks.js"></script>
    <script src="js/fields-validation.js"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>
    	AOS.init();

    	$('#newsletter-modal').modal('show')

    	$("#send").on('click', () => {
  			if(countFilledFields(3)) {
  				alert("Inscrição enviada.");
  			}
    	});
    </script>
  </body>
</html>