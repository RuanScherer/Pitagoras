let breakpoint = $('body').width();
let menuHeight = $('#menu').height();
const SM_BREAKPOINT = 576;
const CLASSES = "fixed-menu shadow-lg";

function setBreakpoint() {
	breakpoint = $('body').width();
}

$(window).on('scroll', function() {
	if(breakpoint > SM_BREAKPOINT) {
    if($(window).scrollTop() >= menuHeight) { 
      $('#menu').addClass(CLASSES);
    }
    else {
      $('#menu').removeClass(CLASSES);
    }
  }
});

$("#menuToggler").on('click', () => {
    $("#menuToggler").addClass("d-none");
  }
);

$("#menuModal").on('click', () => {
    $("#menuToggler").removeClass("d-none");
  }
);