const fields = [...document.getElementsByClassName("form-control")];

function countFilledFields(fieldsCount){
	let count = 0;
	fields.forEach((field) => {
		if(field.value.length > 0) {
			count += 1;
		}
	});
	if(count == fieldsCount) {
		return true;
	}
	return false;
}