let phone = document.querySelector("#phone");
let cpf = document.querySelector("#cpf");

const inputTypeIsDelete = evt => (evt.inputType != "deleteContentBackward");
const lengthIsEqual = (field, test) => (field.value.length == test);

phone.addEventListener('input', (e) => {
  if(lengthIsEqual(phone, 1) && inputTypeIsDelete(e)) {
    phone.value = "(" + phone.value;
  } else if(lengthIsEqual(phone, 3) && inputTypeIsDelete(e)) {
    phone.value += ")";
  } else if(lengthIsEqual(phone, 9) && inputTypeIsDelete(e)) {
    phone.value += "-";
  }
});

if(cpf != null) {
  cpf.addEventListener('input', (e) => {
    if((lengthIsEqual(cpf, 3) || lengthIsEqual(cpf, 7)) && inputTypeIsDelete(e)) {
      cpf.value += ".";
    } else if (lengthIsEqual(cpf, 11) && inputTypeIsDelete(e)) {
      cpf.value += "-";
    }
  });
}