<?php

include_once("../controller/BuyerController.php");
include_once($_SERVER['DOCUMENT_ROOT']."/Pitagoras/scripts/logout.php");
include_once($_SERVER['DOCUMENT_ROOT']."/Pitagoras/scripts/session-doesnt-exist.php");
redirectIfNotLogged("logged", "customer-login.php");

$_GET['id'] = $_SESSION['logged'];
$buyerController = new BuyerController($_GET);

$buyer = $buyerController->getById();

if(isset($_GET['alter']))
{
	$_POST['id'] = $_SESSION['logged'];
	$newBuyer = new BuyerController($_POST);
	if($newBuyer->edit())
	{
		Header("Location: profile.php?status=1");
	}
	else
	{
		Header("Location: profile.php?status=2");
	}
}

?>

<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Visualize e altere as informações da sua conta.">

    <link rel="stylesheet" href="../bootstrap/bootstrap.css">
    <link rel="stylesheet" href="../styles.css">

    <link rel="sortcut icon" href="assets/logo.png" type="image/png"/>

    <!-- AOS Library -->
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

    <title>CFC Pitágoras</title>
  </head>
  <body onresize="setBreakpoint()">
		<button class="btn btn-dark shadow rounded-circle fixed-bottom m-3 d-sm-none left-auto" data-toggle="modal" data-target="#menuModal" id="menuToggler">
			<svg class="bi bi-list" width="2em" height="2em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
			  <path fill-rule="evenodd" d="M2.5 11.5A.5.5 0 013 11h10a.5.5 0 010 1H3a.5.5 0 01-.5-.5zm0-4A.5.5 0 013 7h10a.5.5 0 010 1H3a.5.5 0 01-.5-.5zm0-4A.5.5 0 013 3h10a.5.5 0 010 1H3a.5.5 0 01-.5-.5z" clip-rule="evenodd"/>
			</svg>
		</button>
		<div class="modal fade" id="menuModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog modal-dialog-centered" role="document">
		    <div class="modal-content">
		      <div class="modal-header justify-content-center">
		        <h5 class="modal-title" id="exampleModalLabel">Menu</h5>
		      </div>
		      <div class="modal-body d-flex flex-column align-items-center">
			      <a class="btn btn-link text-dark text-decoration-none d-flex align-items-center" href="../index.php">
			      	<svg class="bi bi-house mr-1" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
							  <path fill-rule="evenodd" d="M2 13.5V7h1v6.5a.5.5 0 00.5.5h9a.5.5 0 00.5-.5V7h1v6.5a1.5 1.5 0 01-1.5 1.5h-9A1.5 1.5 0 012 13.5zm11-11V6l-2-2V2.5a.5.5 0 01.5-.5h1a.5.5 0 01.5.5z" clip-rule="evenodd"/>
							  <path fill-rule="evenodd" d="M7.293 1.5a1 1 0 011.414 0l6.647 6.646a.5.5 0 01-.708.708L8 2.207 1.354 8.854a.5.5 0 11-.708-.708L7.293 1.5z" clip-rule="evenodd"/>
							</svg>
			      	Home
			     	</a>
			      <a class="btn btn-link text-dark text-decoration-none d-flex align-items-center" href="shop.php">
			      	<svg class="bi bi-bag mr-1" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
							  <path fill-rule="evenodd" d="M14 5H2v9a1 1 0 001 1h10a1 1 0 001-1V5zM1 4v10a2 2 0 002 2h10a2 2 0 002-2V4H1z" clip-rule="evenodd"/>
							  <path d="M8 1.5A2.5 2.5 0 005.5 4h-1a3.5 3.5 0 117 0h-1A2.5 2.5 0 008 1.5z"/>
							</svg>
			      	Ofertas
			      </a>
			      <a class="btn btn-link text-dark text-decoration-none d-flex align-items-center" href="news.php">
			      	<svg class="bi bi-file-post mr-1" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
							  <path fill-rule="evenodd" d="M4 1h8a2 2 0 012 2v10a2 2 0 01-2 2H4a2 2 0 01-2-2V3a2 2 0 012-2zm0 1a1 1 0 00-1 1v10a1 1 0 001 1h8a1 1 0 001-1V3a1 1 0 00-1-1H4z" clip-rule="evenodd"/>
							  <path d="M4 5.5a.5.5 0 01.5-.5h7a.5.5 0 01.5.5v7a.5.5 0 01-.5.5h-7a.5.5 0 01-.5-.5v-7z"/>
							  <path fill-rule="evenodd" d="M4 3.5a.5.5 0 01.5-.5h5a.5.5 0 010 1h-5a.5.5 0 01-.5-.5z" clip-rule="evenodd"/>
							</svg>
			      	Novidades</a>
			      <a class="btn btn-link text-dark text-decoration-none d-flex align-items-center" href="contact.php">
			      	<svg class="bi bi-chat-square mr-1" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
							  <path fill-rule="evenodd" d="M14 1H2a1 1 0 00-1 1v8a1 1 0 001 1h2.5a2 2 0 011.6.8L8 14.333 9.9 11.8a2 2 0 011.6-.8H14a1 1 0 001-1V2a1 1 0 00-1-1zM2 0a2 2 0 00-2 2v8a2 2 0 002 2h2.5a1 1 0 01.8.4l1.9 2.533a1 1 0 001.6 0l1.9-2.533a1 1 0 01.8-.4H14a2 2 0 002-2V2a2 2 0 00-2-2H2z" clip-rule="evenodd"/>
							</svg>
			      	Contato
			      </a>
		      </div>
		    </div>
		  </div>
		</div>

		<nav id="menu" class="navbar px-4 py-3 navbar-expand-sm navbar-light br-radius bl-radius w-100 d-flex bg-yellow">
			<div class="collapse navbar-collapse" id="navbarNav">
			  <ul class="navbar-nav">
			  	<li class="nav-item">
			      <a class="nav-link" href="../index.php">Home</a>
			    </li>
			    <li class="nav-item">
			      <a class="nav-link" href="shop.php">Ofertas</a>
			    </li>
			    <li class="nav-item">
			      <a class="nav-link" href="news.php">Novidades</a>
			    </li>
			    <li class="nav-item">
			      <a class="nav-link" href="contact.php">Contato</a>
			    </li>
			  </ul>
			</div>
		</nav>

		<section class="w-100 pt-4 px-4 d-flex flex-column align-items-center">
				<h1 class="font-weight-bold">Perfil</h1>
				<div class="d-flex w-100 my-2 justify-content-end max-w-600">
					<a href="?logout=1" class="btn btn-link font-weight-bold text-decoration-none text-danger">Encerrar sessão</a>
				</div>
				<?php
				if(mysqli_num_rows($buyer))
				{
					while ($row = mysqli_fetch_assoc($buyer)) {
				?>
				<form method="post" action="?alter=1" class="d-flex flex-column text-center align-items-center card w-100 shadow-sm mb-4 p-4" data-aos="fade-up max-w-600">
					<h4 class="text-dark mb-3">Informações Pessoais</h4>
				  <div class="form-group w-100">
				    <label for="name" class="h6 text-dark">Nome</label>
				    <input type="text" class="form-control text-center" id="name" name="name" value="<?php echo $row['name']; ?>" maxlength="100" placeholder="Nome completo" required>
				  </div>
				  <div class="form-group w-100">
				    <label for="cpf" class="h6 text-dark">CPF</label>
				    <input type="text" class="form-control text-center" id="cpf" name="cpf" maxlength="14" value="<?php echo $row['cpf']; ?>" placeholder="000.000.000-00" required>
				  </div>
				  <div class="form-group w-100">
				    <label for="password" class="h6 text-dark">Senha</label>
				    <input type="password" class="form-control text-center" id="password" name="password" value="<?php echo $row['password']; ?>" placeholder="Sua senha secreta" maxlength="40" required>
				  </div>
				  <h4 class="text-dark my-3">Informações de Contato</h4>
				  <div class="form-group w-100">
				    <label for="phone" class="h6 text-dark">Celular</label>
				    <input type="text" class="form-control text-center" id="phone" name="phone" maxlength="14" value="<?php echo $row['phone']; ?>" placeholder="(00)00000-0000" required>
				  </div>
				  <div class="form-group w-100">
				    <label for="email" class="h6 text-dark">Email</label>
				    <input type="email" class="form-control text-center" id="email" name="email" value="<?php echo $row['email']; ?>" maxlength="90" placeholder="usuario@email.com" required>
				  </div>
				  <div class="form-group w-100">
				    <label for="address" class="h6 text-dark">Endereço</label>
				    <input type="text" class="form-control text-center" id="address" name="address" value="<?php echo $row['address']; ?>" maxlength="150" placeholder="Rua, n°, bairro, cidade" required>
				  </div>
				  <?php

						  if(isset($_GET['status']) && $_GET['status'] == 1)
							{
							
					?>

          <p class='alert alert-success fade show w-100' role='alert'>
            Alterações salvas com sucesso!
          </p>
		      
		      <?php
					
							}
							elseif (isset($_GET['status']) && $_GET['status'] == 2) 
							{
					
					?>
		      
          <p class='alert alert-success fade show w-100' role='alert'>
            Erro ao salvar as alterações, tente novamente.
          </p>
		      
		      <?php

							}
					
					?>
					<button id="send" class="btn btn-dark w-100 mt-2 font-weight-bold">Salvar</button>
					<?php

						}
				  }
				  else
				  {
				  
				  ?>
					
					<a class='list-group-item list-group-item-action flex-column align-items-center px-3 py-5 max-w-600' data-aos='fade-up'>
				    <h4 class='font-weight-normal text-muted mb-1 text-center op-08'>Tivemos problemas ao buscar suas informações, volte e tente novamente.</h4>
				  </a>
					
					<?php

				  }

				  ?>
				</form>
		</section>

		<footer class="d-flex justify-content-center py-4 px-4">
			<span class="font-weight-bold text-dark">
				Desenvolvido por
				<a class="link text-decoration-none instagram-color" href="https://www.instagram.com/scherer_programmer/" target="_blank" rel="noopener">
					<strong>Ruan Scherer &#8599;</strong>
				</a>
			</span>
		</footer>

    <script src="../js/jquery.slim.min.js"></script>
    <script src="../js/popper.js"></script>
    <script src="../js/bootstrap.js"></script>
    <script src="../js/menu.js"></script>
    <script src="../js/masks.js"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script type="text/javascript">
    	AOS.init();
    </script>
  </body>
</html>