<?php

include_once("../controller/BuyerController.php");
include_once($_SERVER['DOCUMENT_ROOT']."/Pitagoras/scripts/session-exists.php");
redirectIfLogged("logged", "../index.php");

?>

<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Faça login com sua conta para adquirir nossos serviços de onde estiver.">

    <link rel="stylesheet" href="../bootstrap/bootstrap.css">
    <link rel="stylesheet" href="../styles.css">

    <link rel="sortcut icon" href="assets/logo.png" type="image/png"/>

    <!-- AOS Library -->
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

    <title>CFC Pitágoras</title>
  </head>
  <body>
		<div class="d-flex flex-column align-items-center bg-yellow">
			<aside class="w-100 mh-100vh bg-yellow px-4 py-5 d-flex justify-content-center align-items-center">
        <form method="post" action="?login=1" class="w-100 card b-radius shadow text-left d-flex flex-column align-items-start p-4 p-md-5 max-w-650" data-aos="zoom-in">
          <h2>Entrar</h2>
          <h5 class="text-muted mb-4">Faça login com sua conta já existente.</h5>
          <div class="form-group w-100">
            <label for="email" class="font-weight-bold text-dark">Email</label>
            <input type="text" class="form-control" id="email" name="email" maxlength="90" placeholder="usuario@email.com" required>
          </div>
          <div class="form-group w-100">
            <label for="password" class="font-weight-bold text-dark">Senha</label>
            <input type="password" class="form-control" id="password" name="password" maxlength="40" placeholder="Sua senha secreta" required>
          </div>
          <?php

          if(isset($_GET['login']))
          {
            $buyer = new BuyerController($_POST);
            if($buyer->authenticate())
            {
              Header("Location: ../index.php");
            }
            else
            {
            
            ?>

              <p class='alert alert-danger alert-dismissible fade show w-100' role='alert'>
                Erro ao logar, tente novamente.
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                  <span aria-hidden='true'>&times;</span>
                </button>
              </p>
            
            <?php

            }
          }

          ?>
          <button id="next" class="btn btn-dark align-self-end font-weight-bold mt-2">Entrar</button>
          <a href="#register" class="btn-link text-muted text-center align-self-center mt-4">Não tem uma conta? Cadastre-se agora mesmo!</a>
        </form> 
      </aside>
			<section id="register" class="w-100 tr-radius tl-radius bg-white d-flex flex-column shadow p-4 p-md-5" data-aos="fade-up">
        <h2>Cadastre-se</h2>
				<h5 class="text-muted mb-3">Começe a adquirir nossos serviços de onde estiver.</h5>
				<form method="post" action="?register=1" class="text-left d-flex flex-column align-items-start mt-4">
          <div class="form-group w-100">
            <label for="name" class="font-weight-bold text-dark">Nome Completo</label>
            <input type="text" class="form-control" id="name" name="name" maxlength="100" placeholder="Nome completo" required>
          </div>
          <div class="form-group w-100">
            <label for="cpf" class="font-weight-bold text-dark">CPF</label>
            <input type="text" class="form-control" id="cpf" name="cpf" maxlength="14" placeholder="000.000.000-00" required>
          </div>
          <div class="form-group w-100">
            <label for="email" class="font-weight-bold text-dark">Email</label>
            <input type="email" class="form-control" id="email" name="email" maxlength="90" placeholder="usuario@email.com" required>
          </div>
          <div class="form-group w-100">
            <label for="password" class="font-weight-bold text-dark">Senha</label>
            <input type="password" class="form-control" id="password" name="password" maxlength="40" placeholder="Sua senha secreta" required>
          </div>
          <div class="form-group w-100">
            <label for="phone" class="font-weight-bold text-dark">Celular</label>
            <input type="text" class="form-control" id="phone" name="phone" maxlength="14" placeholder="(00)00000-0000" required>
          </div>
          <div class="form-group w-100">
            <label for="address" class="font-weight-bold text-dark">Endereço</label>
            <input type="text" class="form-control" id="address" name="address" maxlength="150" placeholder="Rua, n°, bairro, cidade" required>
          </div>
          <?php

          if(isset($_GET['register']))
          {
            $buyer = new BuyerController($_POST);
            if($buyer->new())
            {
              $buyer->authenticate();
              Header("Location: shop.php");
            }
            else
            {
              header("Location: customer-login.php?error=1");
            }
          }

          if (isset($_GET['error'])) {
          
          ?>

            <p class='alert alert-danger alert-dismissible fade show w-100' role='alert'>
              Erro ao cadastrar. Verifique se os dados informados são corretos e tente novamente.
              <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button>
            </p>
          
          <?php
          
          }

          ?>
          <button id="next" class="btn btn-dark align-self-end font-weight-bold mt-2">Cadastrar-me</button>
        </form>
			</section>
		</div>

    <script src="../js/jquery.slim.min.js"></script>
    <script src="../js/popper.js"></script>
    <script src="../js/bootstrap.js"></script>
    <script src="../js/masks.js"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script type="text/javascript">
      AOS.init();
    </script>
  </body>
</html>