<?php

include_once("../controller/MessageController.php");
include_once($_SERVER['DOCUMENT_ROOT']."/Pitagoras/scripts/logout.php");

if(isset($_GET['send']))
{
	$message = new MessageController($_POST);
	$message->new();
	Header("Location: contact.php");
}

?>

<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Entre em contato conosco. Deixe sua mensagem ou utilize as redes sociais (Instagram, Facebook). Atendemos também pelo telefone.">

    <link rel="stylesheet" href="../bootstrap/bootstrap.css">
    <link rel="stylesheet" href="../styles.css">

    <link rel="sortcut icon" href="assets/logo.png" type="image/png"/>

    <!-- AOS Library -->
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

    <title>CFC Pitágoras</title>
  </head>
  <body onresize="setBreakpoint()">
		<button class="btn btn-dark shadow rounded-circle fixed-bottom m-3 d-sm-none left-auto" data-toggle="modal" data-target="#menuModal" id="menuToggler">
			<svg class="bi bi-list" width="2em" height="2em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
			  <path fill-rule="evenodd" d="M2.5 11.5A.5.5 0 013 11h10a.5.5 0 010 1H3a.5.5 0 01-.5-.5zm0-4A.5.5 0 013 7h10a.5.5 0 010 1H3a.5.5 0 01-.5-.5zm0-4A.5.5 0 013 3h10a.5.5 0 010 1H3a.5.5 0 01-.5-.5z" clip-rule="evenodd"/>
			</svg>
		</button>
		<div class="modal fade" id="menuModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog modal-dialog-centered" role="document">
		    <div class="modal-content">
		      <div class="modal-header justify-content-center">
		        <h5 class="modal-title" id="exampleModalLabel">Menu</h5>
		      </div>
		      <div class="modal-body d-flex flex-column align-items-center">
			      <a class="btn btn-link text-dark text-decoration-none d-flex align-items-center" href="../index.php">
			      	<svg class="bi bi-house mr-1" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
							  <path fill-rule="evenodd" d="M2 13.5V7h1v6.5a.5.5 0 00.5.5h9a.5.5 0 00.5-.5V7h1v6.5a1.5 1.5 0 01-1.5 1.5h-9A1.5 1.5 0 012 13.5zm11-11V6l-2-2V2.5a.5.5 0 01.5-.5h1a.5.5 0 01.5.5z" clip-rule="evenodd"/>
							  <path fill-rule="evenodd" d="M7.293 1.5a1 1 0 011.414 0l6.647 6.646a.5.5 0 01-.708.708L8 2.207 1.354 8.854a.5.5 0 11-.708-.708L7.293 1.5z" clip-rule="evenodd"/>
							</svg>
			      	Home
			     	</a>
			      <a class="btn btn-link text-dark text-decoration-none d-flex align-items-center" href="shop.php">
			      	<svg class="bi bi-bag mr-1" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
							  <path fill-rule="evenodd" d="M14 5H2v9a1 1 0 001 1h10a1 1 0 001-1V5zM1 4v10a2 2 0 002 2h10a2 2 0 002-2V4H1z" clip-rule="evenodd"/>
							  <path d="M8 1.5A2.5 2.5 0 005.5 4h-1a3.5 3.5 0 117 0h-1A2.5 2.5 0 008 1.5z"/>
							</svg>
			      	Ofertas
			      </a>
			      <a class="btn btn-link text-dark text-decoration-none d-flex align-items-center" href="news.php">
			      	<svg class="bi bi-file-post mr-1" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
							  <path fill-rule="evenodd" d="M4 1h8a2 2 0 012 2v10a2 2 0 01-2 2H4a2 2 0 01-2-2V3a2 2 0 012-2zm0 1a1 1 0 00-1 1v10a1 1 0 001 1h8a1 1 0 001-1V3a1 1 0 00-1-1H4z" clip-rule="evenodd"/>
							  <path d="M4 5.5a.5.5 0 01.5-.5h7a.5.5 0 01.5.5v7a.5.5 0 01-.5.5h-7a.5.5 0 01-.5-.5v-7z"/>
							  <path fill-rule="evenodd" d="M4 3.5a.5.5 0 01.5-.5h5a.5.5 0 010 1h-5a.5.5 0 01-.5-.5z" clip-rule="evenodd"/>
							</svg>
			      	Novidades</a>
			      <a class="btn btn-link text-dark text-decoration-none d-flex align-items-center" href="contact.php">
			      	<svg class="bi bi-chat-square mr-1" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
							  <path fill-rule="evenodd" d="M14 1H2a1 1 0 00-1 1v8a1 1 0 001 1h2.5a2 2 0 011.6.8L8 14.333 9.9 11.8a2 2 0 011.6-.8H14a1 1 0 001-1V2a1 1 0 00-1-1zM2 0a2 2 0 00-2 2v8a2 2 0 002 2h2.5a1 1 0 01.8.4l1.9 2.533a1 1 0 001.6 0l1.9-2.533a1 1 0 01.8-.4H14a2 2 0 002-2V2a2 2 0 00-2-2H2z" clip-rule="evenodd"/>
							</svg>
			      	Contato
			      </a>
		      </div>
		    </div>
		  </div>
		</div>

		<nav id="menu" class="navbar px-4 py-3 navbar-expand-sm navbar-light w-100 d-flex bg-yellow bl-radius br-radius justify-content-end justify-content-sm-between">
			<div class="collapse navbar-collapse" id="navbarNav">
			  <ul class="navbar-nav">
			  	<li class="nav-item">
			      <a class="nav-link" href="../index.php">Home</a>
			    </li>
			    <li class="nav-item">
			      <a class="nav-link" href="shop.php">Ofertas</a>
			    </li>
			    <li class="nav-item">
			      <a class="nav-link" href="news.php">Novidades</a>
			    </li>
			    <li class="nav-item">
			      <a class="nav-link" href="contact.php">Contato</a>
			    </li>
			  </ul>
			</div>
	    <?php

	    if (isset($_SESSION['logged'])) {
	    
	    ?>
	    	<span class='nav-item dropdown'>
	        <a class='nav-link dropdown-toggle text-dark h6' href='#' id='navbarDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
	          <?php echo $_SESSION['buyerName']; ?>
	        </a>
	        <div class='dropdown-menu dropdown-menu-right' aria-labelledby='navbarDropdown'>
	          <a class='dropdown-item d-flex align-items-center' href='profile.php'>
	          	<svg class='bi bi-people-circle mr-1' width='1em' height='1em' viewBox='0 0 16 16' fill='currentColor' xmlns='http://www.w3.org/2000/svg'>
							  <path d='M13.468 12.37C12.758 11.226 11.195 10 8 10s-4.757 1.225-5.468 2.37A6.987 6.987 0 008 15a6.987 6.987 0 005.468-2.63z'/>
							  <path fill-rule='evenodd' d='M8 9a3 3 0 100-6 3 3 0 000 6z' clip-rule='evenodd'/>
							  <path fill-rule='evenodd' d='M8 1a7 7 0 100 14A7 7 0 008 1zM0 8a8 8 0 1116 0A8 8 0 010 8z' clip-rule='evenodd'/>
							</svg>
	          	Meu perfil
	          </a>
	          <div class='dropdown-divider'></div>
	          <a class='dropdown-item text-danger d-flex align-items-center' href='?logout=1'>
	          	<svg class='bi bi-box-arrow-in-left mr-1' width='1em' height='1em' viewBox='0 0 16 16' fill='currentColor' xmlns='http://www.w3.org/2000/svg'>
							  <path fill-rule='evenodd' d='M7.854 11.354a.5.5 0 000-.708L5.207 8l2.647-2.646a.5.5 0 10-.708-.708l-3 3a.5.5 0 000 .708l3 3a.5.5 0 00.708 0z' clip-rule='evenodd'/>
							  <path fill-rule='evenodd' d='M15 8a.5.5 0 00-.5-.5h-9a.5.5 0 000 1h9A.5.5 0 0015 8z' clip-rule='evenodd'/>
							  <path fill-rule='evenodd' d='M2.5 14.5A1.5 1.5 0 011 13V3a1.5 1.5 0 011.5-1.5h8A1.5 1.5 0 0112 3v1.5a.5.5 0 01-1 0V3a.5.5 0 00-.5-.5h-8A.5.5 0 002 3v10a.5.5 0 00.5.5h8a.5.5 0 00.5-.5v-1.5a.5.5 0 011 0V13a1.5 1.5 0 01-1.5 1.5h-8z' clip-rule='evenodd'/>
							</svg>
	          	Sair
	          </a>
	        </div>
	      </span>
	    <?php

	    }

	    ?>
		</nav>

		<section class="container fluid pt-4 px-4">
				<h1 class="col-12">Contato</h1>
				<div class="d-flex flex-wrap">
					<div class="col-lg-6 d-flex flex-column my-2" data-aos="fade-up">
						<h4 class="text-muted mb-1">Alguma dúvida? Quer nos falar algo?</h4>
						<form method="post" action="?send=1" class=" text-left d-flex flex-column align-items-start card shadow-sm my-3 p-4">
						  <div class="form-group w-100">
						    <label for="email" class="font-weight-bold text-dark">Email</label>
						    <input type="email" class="form-control" id="email" name="email" maxlength="90" placeholder="usuario@email.com" required>
						  </div>
						  <div class="form-group w-100">
						    <label for="name" class="font-weight-bold text-dark">Nome</label>
						    <input type="text" class="form-control" id="name" name="name" maxlength="100" placeholder="Nome completo" required>
						  </div>
						  <div class="form-group w-100">
						    <label for="topic" class="font-weight-bold text-dark">Assunto</label>
						    <input type="text" class="form-control" id="topic" name="topic" maxlength="45" placeholder="Assunto da mensagem" required>
						  </div>
						  <div class="form-group w-100">
						    <label for="message" class="font-weight-bold text-dark" spellcheck="on">Mensagem</label>
						    <textarea class="form-control" id="message" rows="3" name="message" maxlength="150" placeholder="Digite aqui..." required></textarea>
						  </div>
						  <button id="send" class="btn btn-dark align-self-end font-weight-bold">Enviar</button>
						</form>
					</div>

					<aside class="col-lg-6 my-2" data-aos="fade-up">
						<h4 class="text-muted mb-1">Outros canais de comunicação...</h4>
						<div class="d-flex flex-column align-items-center align-items-lg-start">
							<a href="https://www.facebook.com/AutoescolaPitagoras/?ref=settings" target="_blank" rel="noopener" class="card my-3 p-2 w-100 card-max shadow-sm text-center text-decoration-none" style="background-color: rgba(66, 103, 178, 0.08);">
							  <div class="card-body">
							    <h5 class="card-title font-weight-bold facebook-color">Facebook &#8599;</h5>
							    <h5 class="card-subtitle mb-2 text-muted">/AutoescolaPitagoras</h5>
							  </div>
							</a>
							<a href="https://www.instagram.com/autoescola_pitagoras/" target="_blank" rel="noopener" class="card my-3 p-2 w-100 card-max shadow-sm text-center text-decoration-none" style="background-color: rgba(241, 76, 90, 0.08);">
							  <div class="card-body">
							    <h5 class="card-title font-weight-bold instagram-color">Instagram &#8599;</h5>
							    <h5 class="card-subtitle mb-2 text-muted">@autoescola_pitagoras</h5>
							  </div>
							</a>
							<a href="https://api.whatsapp.com/send?phone=554732127834" target="_blank" rel="noopener" class="card my-3 p-2 w-100 card-max shadow-sm text-center text-decoration-none" style="background-color: rgba(6, 215, 85, 0.08);">
							  <div class="card-body">
							    <h5 class="card-title font-weight-bold" style="color: #06d755;">WhatsApp &#8599;</h5>
							    <h5 class="card-subtitle mb-2 text-muted">(47) 32127834</h5>
							  </div>
							</a>
						</div>
					</aside>
				</div>
		</section>

		<section class="d-flex flex-wrap justify-content-center pt-3 pb-2 px-4">
			<div class="card m-3 py-3 w-100 col-sm-6 shadow-sm text-center card-max" data-aos="zoom-in">
			  <div class="card-body">
			    <h3 class="card-title d-flex justify-content-center align-items-center">
			    	<svg class="bi bi-geo-alt mr-1" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
						  <path fill-rule="evenodd" d="M8 16s6-5.686 6-10A6 6 0 002 6c0 4.314 6 10 6 10zm0-7a3 3 0 100-6 3 3 0 000 6z" clip-rule="evenodd"/>
						</svg>
			    	Localização
			    </h3>
			    <h5 class="card-subtitle mb-2 text-muted">Onde nos encontrar?</h5>
			    <p class="card-text">Rua General Osório, 3735 </br> Água Verde, Blumenau - SC</p>
			    <a href="https://goo.gl/maps/pG9DMfrmQGfnAzn36" target="_blank" rel="noopener" class="btn btn-link text-decoration-none text-muted">Ir para o mapa</a>
			  </div>
			</div>
		</section>

		<footer class="d-flex justify-content-center py-4 px-4">
			<span class="font-weight-bold text-dark">
				Desenvolvido por
				<a class="link text-decoration-none instagram-color" href="https://www.instagram.com/scherer_programmer/" target="_blank" rel="noopener">
					<strong>Ruan Scherer &#8599;</strong>
				</a>
			</span>
		</footer>

    <script src="../js/jquery.slim.min.js"></script>
    <script src="../js/popper.js"></script>
    <script src="../js/bootstrap.js"></script>
    <script src="../js/menu.js"></script>
    <script src="../js/fields-validation.js"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script type="text/javascript">
    	AOS.init();

    	$("#send").on('click', () => {
  			if(countFilledFields(4)) {
  				alert("Mensagem enviada.");
  			}
    	});
    </script>
  </body>
</html>