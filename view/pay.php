<?php

setcookie("cart", null, -1);

include_once("../controller/BuyController.php");
include_once("../controller/BuyerController.php");
include_once($_SERVER['DOCUMENT_ROOT']."/Pitagoras/scripts/logout.php");
include_once($_SERVER['DOCUMENT_ROOT']."/Pitagoras/scripts/session-doesnt-exist.php");
redirectIfNotLogged("logged", "customer-login.php");

$_POST['buyer'] = $_SESSION['logged'];
$buyController = new BuyController($_POST);

$buy = $buyController->getLast();

$total = 0;
$buyId = 0;
$buyerId = 0;

while ($row = mysqli_fetch_assoc($buy)) {
	$buyId = $row['idtb_buy'];
	$total = $row['total'];
	$buyerId = $row['buyer'];
}

$_POST['id'] = $_SESSION['logged'];
$buyerController = new BuyerController($_POST);

$name = "";
$email = "";
$phone = "";
$cpf = "";

$buyer = $buyerController->getById();

while ($row = mysqli_fetch_assoc($buyer)) {
	$name = explode(" ", $row['name']);
	$email = $row['email'];
	$phone = $row['phone'];
	$cpf = $row['cpf'];
}

?>

<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Escolha uma forma de pagamento e finalize sua compra.">

    <link rel="stylesheet" href="../bootstrap/bootstrap.css">
    <link rel="stylesheet" href="../styles.css">

    <link rel="sortcut icon" href="assets/logo.png" type="image/png"/>

    <title>CFC Pitágoras</title>

    <style type="text/css">
    	.picpay-button:focus {
				border-color: #00C869 !important;
				box-shadow: 0 0 0 0.2rem rgba(0, 200, 105,.25) !important;
    	}
    	.picpay-button {
    		background-color: #00C869 !important
    	}
    </style>
  </head>
  <body onresize="setBreakpoint()">
		<button class="btn btn-dark shadow rounded-circle fixed-bottom m-3 d-sm-none left-auto" data-toggle="modal" data-target="#menuModal" id="menuToggler">
			<svg class="bi bi-list" width="2em" height="2em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
			  <path fill-rule="evenodd" d="M2.5 11.5A.5.5 0 013 11h10a.5.5 0 010 1H3a.5.5 0 01-.5-.5zm0-4A.5.5 0 013 7h10a.5.5 0 010 1H3a.5.5 0 01-.5-.5zm0-4A.5.5 0 013 3h10a.5.5 0 010 1H3a.5.5 0 01-.5-.5z" clip-rule="evenodd"/>
			</svg>
		</button>
		<div class="modal fade" id="menuModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog modal-dialog-centered" role="document">
		    <div class="modal-content">
		      <div class="modal-header justify-content-center">
		        <h5 class="modal-title" id="exampleModalLabel">Menu</h5>
		      </div>
		      <div class="modal-body d-flex flex-column align-items-center">
			      <a class="btn btn-link text-dark text-decoration-none d-flex align-items-center" href="../index.php">
			      	<svg class="bi bi-house mr-1" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
							  <path fill-rule="evenodd" d="M2 13.5V7h1v6.5a.5.5 0 00.5.5h9a.5.5 0 00.5-.5V7h1v6.5a1.5 1.5 0 01-1.5 1.5h-9A1.5 1.5 0 012 13.5zm11-11V6l-2-2V2.5a.5.5 0 01.5-.5h1a.5.5 0 01.5.5z" clip-rule="evenodd"/>
							  <path fill-rule="evenodd" d="M7.293 1.5a1 1 0 011.414 0l6.647 6.646a.5.5 0 01-.708.708L8 2.207 1.354 8.854a.5.5 0 11-.708-.708L7.293 1.5z" clip-rule="evenodd"/>
							</svg>
			      	Home
			     	</a>
			      <a class="btn btn-link text-dark text-decoration-none d-flex align-items-center" href="shop.php">
			      	<svg class="bi bi-bag mr-1" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
							  <path fill-rule="evenodd" d="M14 5H2v9a1 1 0 001 1h10a1 1 0 001-1V5zM1 4v10a2 2 0 002 2h10a2 2 0 002-2V4H1z" clip-rule="evenodd"/>
							  <path d="M8 1.5A2.5 2.5 0 005.5 4h-1a3.5 3.5 0 117 0h-1A2.5 2.5 0 008 1.5z"/>
							</svg>
			      	Ofertas
			      </a>
			      <a class="btn btn-link text-dark text-decoration-none d-flex align-items-center" href="news.php">
			      	<svg class="bi bi-file-post mr-1" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
							  <path fill-rule="evenodd" d="M4 1h8a2 2 0 012 2v10a2 2 0 01-2 2H4a2 2 0 01-2-2V3a2 2 0 012-2zm0 1a1 1 0 00-1 1v10a1 1 0 001 1h8a1 1 0 001-1V3a1 1 0 00-1-1H4z" clip-rule="evenodd"/>
							  <path d="M4 5.5a.5.5 0 01.5-.5h7a.5.5 0 01.5.5v7a.5.5 0 01-.5.5h-7a.5.5 0 01-.5-.5v-7z"/>
							  <path fill-rule="evenodd" d="M4 3.5a.5.5 0 01.5-.5h5a.5.5 0 010 1h-5a.5.5 0 01-.5-.5z" clip-rule="evenodd"/>
							</svg>
			      	Novidades</a>
			      <a class="btn btn-link text-dark text-decoration-none d-flex align-items-center" href="contact.php">
			      	<svg class="bi bi-chat-square mr-1" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
							  <path fill-rule="evenodd" d="M14 1H2a1 1 0 00-1 1v8a1 1 0 001 1h2.5a2 2 0 011.6.8L8 14.333 9.9 11.8a2 2 0 011.6-.8H14a1 1 0 001-1V2a1 1 0 00-1-1zM2 0a2 2 0 00-2 2v8a2 2 0 002 2h2.5a1 1 0 01.8.4l1.9 2.533a1 1 0 001.6 0l1.9-2.533a1 1 0 01.8-.4H14a2 2 0 002-2V2a2 2 0 00-2-2H2z" clip-rule="evenodd"/>
							</svg>
			      	Contato
			      </a>
		      </div>
		    </div>
		  </div>
		</div>

		<nav id="menu" class="navbar px-4 py-3 navbar-expand-sm navbar-light w-100 d-flex bg-yellow bl-radius br-radius justify-content-end justify-content-sm-between">
			<div class="collapse navbar-collapse" id="navbarNav">
			  <ul class="navbar-nav">
			  	<li class="nav-item">
			      <a class="nav-link" href="../index.php">Home</a>
			    </li>
			    <li class="nav-item">
			      <a class="nav-link" href="shop.php">Ofertas</a>
			    </li>
			    <li class="nav-item">
			      <a class="nav-link" href="news.php">Novidades</a>
			    </li>
			    <li class="nav-item">
			      <a class="nav-link" href="contact.php">Contato</a>
			    </li>
			  </ul>
			</div>
	    <?php

	    if (isset($_SESSION['logged'])) {
	    
	    ?>
	    	<span class='nav-item dropdown'>
	        <a class='nav-link dropdown-toggle text-dark h6' href='#' id='navbarDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
	          <?php echo $_SESSION['buyerName']; ?>
	        </a>
	        <div class='dropdown-menu dropdown-menu-right' aria-labelledby='navbarDropdown'>
	          <a class='dropdown-item d-flex align-items-center' href='profile.php'>
	          	<svg class='bi bi-people-circle mr-1' width='1em' height='1em' viewBox='0 0 16 16' fill='currentColor' xmlns='http://www.w3.org/2000/svg'>
							  <path d='M13.468 12.37C12.758 11.226 11.195 10 8 10s-4.757 1.225-5.468 2.37A6.987 6.987 0 008 15a6.987 6.987 0 005.468-2.63z'/>
							  <path fill-rule='evenodd' d='M8 9a3 3 0 100-6 3 3 0 000 6z' clip-rule='evenodd'/>
							  <path fill-rule='evenodd' d='M8 1a7 7 0 100 14A7 7 0 008 1zM0 8a8 8 0 1116 0A8 8 0 010 8z' clip-rule='evenodd'/>
							</svg>
	          	Meu perfil
	          </a>
	          <div class='dropdown-divider'></div>
	          <a class='dropdown-item text-danger d-flex align-items-center' href='?logout=1'>
	          	<svg class='bi bi-box-arrow-in-left mr-1' width='1em' height='1em' viewBox='0 0 16 16' fill='currentColor' xmlns='http://www.w3.org/2000/svg'>
							  <path fill-rule='evenodd' d='M7.854 11.354a.5.5 0 000-.708L5.207 8l2.647-2.646a.5.5 0 10-.708-.708l-3 3a.5.5 0 000 .708l3 3a.5.5 0 00.708 0z' clip-rule='evenodd'/>
							  <path fill-rule='evenodd' d='M15 8a.5.5 0 00-.5-.5h-9a.5.5 0 000 1h9A.5.5 0 0015 8z' clip-rule='evenodd'/>
							  <path fill-rule='evenodd' d='M2.5 14.5A1.5 1.5 0 011 13V3a1.5 1.5 0 011.5-1.5h8A1.5 1.5 0 0112 3v1.5a.5.5 0 01-1 0V3a.5.5 0 00-.5-.5h-8A.5.5 0 002 3v10a.5.5 0 00.5.5h8a.5.5 0 00.5-.5v-1.5a.5.5 0 011 0V13a1.5 1.5 0 01-1.5 1.5h-8z' clip-rule='evenodd'/>
							</svg>
	          	Sair
	          </a>
	        </div>
	      </span>
	   	
	   	<?php
	    
	    }

	    ?>
		</nav>

		<section id="shop" class="container fluid pt-4">
				<h1 class="col-12 mb-md-0 mb-3">Finalizar Compra</h1>
				<div class="d-flex flex-wrap mt-3">
					<div class="col-lg-8 d-flex flex-column my-2">
						<h4 class="text-dark mb-1">Forma de pagamento</h4>
						<h6 class="text-muted mb-1">No momento essas são as opções disponíveis para pagamento.</h6>
						<div class='w-100 mt-3 d-flex flex-column'>
							<div class="card my-2 p-3 w-100 shadow-sm text-center" style="background-color: rgba(0, 200, 105, 0.08)">
							  <div class="card-body">
							    <h3 class="card-title">PicPay</h3>
							    <h5 class="card-subtitle mb-2 text-muted">Cartão de crédito ou Depósito Bancário</h5>
							    <p class="card-text">
							    	Caso não tenha uma conta no aplicativo 
							    	<a href="https://picpay.com/site/download?shortlink=df2d0c2e&pid=Site" target="_blank" class="font-weight-bold text-decoration-none" rel="noopener" style="color: #00C869">clique aqui</a> 
							    	para baixar e criar uma conta em poucos minutos.
							   	</p>
							   	<button id="picpayButton" class="btn w-100 text-light picpay-button">Pagar com PicPay</button>
							  </div>
							</div>
							<div class="card my-2 p-3 w-100 shadow-sm text-center" style="background-color: rgba(0, 168, 215, 0.08)">
							  <div class="card-body">
							    <h3 class="card-title">Viacredi - AILOS</h3>
							    <h5 class="card-subtitle mb-2 text-muted">Depósito bancário</h5>
							    <p class="card-text">
							    	Agência: 001</br>
							    	Banco: 085</br>
							    	Conta: 4059530</br>
							    	CNPJ: 24.140.472/0001-08
							    </p>
							  </div>
							</div>
							<div class="card my-2 p-3 w-100 shadow-sm text-center" style="background-color: rgba(0, 166, 149, 0.08)">
							  <div class="card-body">
							    <h3 class="card-title"> Sicoob</h3>
							    <h5 class="card-subtitle mb-2 text-muted">Depósito bancário</h5>
							    <p class="card-text">
							    	Agência: 3305</br>
							    	Banco: 756</br>
							    	Conta: 4196-3</br>
							    	CNPJ: 24.140.472/0001-08
							    </p>
							  </div>
							</div>
						</div>
					</div>

					<aside class="col-lg-4 my-2">
						<h4 class="text-dark mb-1 text-left">Processo de compra</h4>
						<h6 class="text-muted mb-1">Etapas necessárias para concluir a compra.</h6>
						<div class="card py-1 mt-3 w-100 shadow-sm op-08">
						  <div class="card-body">
						  	<h5 class="mb-2 text-dark">Conferir carrinho</h5>
						  	<h6 class="text-muted mb-2">Valor total: R$<?php echo number_format($total, 2, ',', '.'); ?></h6>
						  </div>
						</div>
						<div class="card py-0 mt-3 w-100 shadow-sm">
						  <div class="card-body">
						    <h5 class="mb-2 text-dark">Pagamento</h5>
						    <h6 class="text-muted mb-1">Escolher forma de pagamento e finalizar compra.</h6>
						  </div>
						</div>
					</aside>
				</div>
		</section>

		<footer class="d-flex justify-content-center py-4 px-4">
			<span class="font-weight-bold text-dark">
				Desenvolvido por
				<a class="link text-decoration-none instagram-color" href="https://www.instagram.com/scherer_programmer/" target="_blank" rel="noopener">
					<strong>Ruan Scherer &#8599;</strong>
				</a>
			</span>
		</footer>

    <script src="../js/jquery.slim.min.js"></script>
    <script src="../js/popper.js"></script>
    <script src="../js/bootstrap.js"></script>
    <script src="../js/menu.js"></script>
    <script
    	src="https://code.jquery.com/jquery-3.4.1.min.js"
		  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
		  crossorigin="anonymous"></script>
    <script type="text/javascript">
  		$("#picpayButton").on('click', () => {
  			$.ajax({
  				method: "post",
				  url: "https://appws.picpay.com/ecommerce/public/payments",
				  data: {
				  	"referenceId": "<?php echo $buyId; ?>",
				  	"callbackUrl": "http://cfcpitagoras.com/payment.php",
				  	"value": <?php echo $total; ?>,
				  	"buyer": {
							"firstName": "<?php echo $name[0]; ?>",
							"lastName" : "<?php echo $name[count($name) - 1] ?>",
							"document": "<?php echo $cpf; ?>",
							"email" : "<?php echo $email; ?>",
							"phone": "<?php echo $phone; ?>"
						}
				  },
				  headers: {
				  	'x-picpay-token' : "727cdb42-36e8-4929-bdea-091967d7d1b9"
				  },
				  statusCode: {
				    401: function() {
				      alert( "Erro ao iniciar compra." );
				    },
				    200: function() {
				      alert( "Compra iniciada com sucesso." );
				    },
				    422: function() {
				      alert( "Erro de validação dos dados." );
				    },
				    500: function() {
				      alert( "Erro nos servidores do picpay, tente novamente mais tarde." );
				    }
				  }
				})
  		});
    </script>
  </body>
</html>