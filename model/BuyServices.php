<?php 

include_once("Connection.php");

class BuyServices extends Connection
{
	private $services;
	private $buy;

	public function __construct($post)
	{
		if(isset($post['services']))
		{
			$this->services = $post['services'];
		}
		if(isset($post['buy']))
		{
			$this->buy = $post['buy'];
		}
	}

	public function __get($prop)
	{
		return $this->$prop;
	}

	public function __set($prop, $value)
	{
		$this->prop = $value;
	}

	public function connect()
	{
		return $this->getConnection();
	}

}

?>