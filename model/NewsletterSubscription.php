<?php 

include_once("Connection.php");

class NewsletterSubscription extends Connection
{
	
	private $id;
	private $name;
	private $email;
	private $phone;

	public function __construct($post)
	{
		if(isset($post['id']))
		{
			$this->id = $post['id'];
		}
		if(isset($post['name']))
		{
			$this->name = $post['name'];
		}
		if(isset($post['email']))
		{
			$this->email = $post['email'];
		}

		if(isset($post['phone']))
		{
			$this->phone = $post['phone'];
		}
	}

	public function __get($prop)
	{
		return $this->$prop;
	}

	public function __set($prop, $value)
	{
		$this->prop = $value;
	}

	public function connect()
	{
		return $this->getConnection();
	}

}

?>