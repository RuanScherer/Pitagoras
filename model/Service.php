<?php 

include_once("Connection.php");

class Service extends Connection
{
	
	private $id;
	private $name;
	private $price;
	private $details;
	private $description;

	public function __construct($post)
	{
		if(isset($post['id']))
		{
			$this->id = $post['id'];
		}
		if(isset($post['name']))
		{
			$this->name = $post['name'];
		}
		if(isset($post['price']))
		{
			$this->price = $post['price'];
		}
		if(isset($post['details']))
		{
			$this->details = $post['details'];
		}
		if(isset($post['description']))
		{
			$this->description = $post['description'];
		}
	}

	public function __get($prop)
	{
		return $this->$prop;
	}

	public function __set($prop, $value)
	{
		$this->prop = $value;
	}

	public function connect()
	{
		return $this->getConnection();
	}

}

?>