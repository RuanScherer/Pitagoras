<?php 

include_once("Connection.php");

class Buy extends Connection
{
	
	private $id;
	private $total;
	private $buyer;

	public function __construct($post)
	{
		if(isset($post['id']))
		{
			$this->id = $post['id'];
		}
		if(isset($post['total']))
		{
			$this->total = $post['total'];
		}
		if(isset($post['buyer']))
		{
			$this->buyer = $post['buyer'];
		}
	}

	public function __get($prop)
	{
		return $this->$prop;
	}

	public function __set($prop, $value)
	{
		$this->prop = $value;
	}

	public function connect()
	{
		return $this->getConnection();
	}

}

?>