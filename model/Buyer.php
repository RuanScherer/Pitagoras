<?php 

include_once("Connection.php");

class Buyer extends Connection
{
	
	private $id;
	private $name;
	private $phone;
	private $email;
	private $cpf;
	private $address;
	private $password;

	public function __construct($post)
	{
		if(isset($post['id']))
		{
			$this->id = $post['id'];
		}
		if(isset($post['name']))
		{
			$this->name = $post['name'];
		}
		if(isset($post['phone']))
		{
			$this->phone = $post['phone'];
		}
		if(isset($post['email']))
		{
			$this->email = $post['email'];
		}
		if(isset($post['cpf']))
		{
			$this->cpf = $post['cpf'];
		}
		if(isset($post['address']))
		{
			$this->address = $post['address'];
		}
		if(isset($post['password']))
		{
			$this->password = $post['password'];
		}
	}

	public function __get($prop)
	{
		return $this->$prop;
	}

	public function __set($prop, $value)
	{
		$this->prop = $value;
	}

	public function connect()
	{
		return $this->getConnection();
	}

}

?>