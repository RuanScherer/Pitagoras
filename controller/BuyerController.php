<?php 

include_once($_SERVER['DOCUMENT_ROOT']."/Pitagoras/model/Buyer.php");

class BuyerController extends Buyer
{

	public function __construct($post)
	{
		parent::__construct($post);
	}

	// Create a new buyer
	public function new()
	{
		$query = "insert into tb_buyer(name, phone, email, cpf, address, password) values('".$this->name."', '".$this->phone."', '".$this->email."', '".$this->cpf."', '".$this->address."', '".$this->password."')";
		$response = mysqli_query($this->connect(), $query);

		if($response)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	// Search for all buyers
	public function getAll()
	{
		$query = "select * from tb_buyer";
		return mysqli_query($this->connect(), $query);
	}

	// Search by name
	public function getByName()
	{
		$query = "
			select br.*, b.total, b.idtb_buy
			from tb_buyer br
			inner join tb_buy b
			on idtb_buyer = b.buyer
			where br.name like '%".$this->name."%'";
		if (strlen($this->name) == 0)
		{
			return $this->getCompleteBuy();
		}
		else
		{
			return mysqli_query($this->connect(), $query);
		}
	}

	// Search by id
	public function getById()
	{
		$query = "select * from tb_buyer where idtb_buyer = ".$this->id;
		return mysqli_query($this->connect(), $query);
	}

	// Get buyer and buy data
	public function getCompleteBuy()
	{
		$query = "
			select br.*, b.total, b.idtb_buy
			from tb_buyer br
			inner join  tb_buy b
			on idtb_buyer = b.buyer;";
		return mysqli_query($this->connect(), $query);
	}

	// Authenticate an buyer
	public function authenticate()
	{
		$query = "select idtb_buyer, name from tb_buyer where email = '".$this->email."' and password = '".$this->password."'";
		$response = mysqli_query($this->connect(), $query);

		if(@mysqli_num_rows($response) >= 1)
		{
			session_start();
			while($row = mysqli_fetch_assoc($response))
			{
				$firstName = explode(" ", $row['name']);
				$_SESSION['logged'] = $row['idtb_buyer'];
				$_SESSION['buyerName'] = $row['name'];
				$_SESSION['buyerName'] = $firstName[0];
			}
			return true;
		}
		else
		{
			return false;
		}
	}

	// Edit a buyer record
	public function edit()
	{
		$query = "update tb_buyer set name = '".$this->name."', cpf = '".$this->cpf."', password = '".$this->password."', phone = '".$this->phone."', email = '".$this->email."', address = '".$this->address."' where idtb_buyer = ".$this->id;
		$response = mysqli_query($this->connect(), $query);
		if($response)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

}

?>