<?php 

include_once($_SERVER['DOCUMENT_ROOT']."/Pitagoras/model/NewsletterSubscription.php");

class NewsletterSubscriptionController extends NewsletterSubscription
{

	public function __construct($post)
	{
		parent::__construct($post);
	}

	// Create a new subscription
	public function new()
	{
		$query = "insert into tb_newsletter_subscription(name, email, phone, readed, subscriptionDate) values('".$this->name."', '".$this->email."', '".$this->phone."', '0', date(now()))";
		$response = mysqli_query($this->connect(), $query);

		if($response)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	// Search for all subscription
	public function getAll()
	{
		$query = "select * from tb_newsletter_subscription";
		return mysqli_query($this->connect(), $query);
	}

	// Search by name
	public function getByName()
	{
		$query = "select * from tb_newsletter_subscription where name like '%".$this->name."%'";
		if (strlen($this->name) == 0)
		{
			return $this->getAll();
		}
		else
		{
			return mysqli_query($this->connect(), $query);
		}
	}

	//Autodestruct subscriptions with more than 15 days
	public function autodestruct()
	{
		$query = "delete from tb_newsletter_subscription where (datediff(date(now()), date)) >= 15";
		mysqli_query($this->connect(), $query);
	}

	// Set all subscription as read
	public function setReaded() {
		$query = "update tb_newsletter_subscription set readed = '1'";
		$response = mysqli_query($this->connect(), $query);
	}

	// Get total unreaded subscriptions
	public function getUnreaded()
	{
		$query = "select count(idtb_subscription) as 'unreaded' from tb_newsletter_subscription where readed = 0";
		return mysqli_query($this->connect(), $query);
	}

}

?>