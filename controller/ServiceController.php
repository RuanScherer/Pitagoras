<?php 

include_once($_SERVER['DOCUMENT_ROOT']."/Pitagoras/model/Service.php");

class ServiceController extends Service
{

	public function __construct($post)
	{
		parent::__construct($post);
	}

	// Create a new service
	public function new()
	{
		$query = "insert into tb_service(name, price, details, description) values('".$this->name."', '".$this->price."', '".$this->details."', '".$this->description."')";
		$response = mysqli_query($this->connect(), $query);

		if($response)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	// Search for all service
	public function getAll()
	{
		$query = "select * from tb_service order by idtb_service desc";
		return mysqli_query($this->connect(), $query);
	}

	// Search by name
	public function getByName()
	{
		$query = "select * from tb_service where name like '%".$this->name."%'";
		if (strlen($this->name) == 0)
		{
			return $this->getAll();
		}
		else
		{
			return mysqli_query($this->connect(), $query);
		}
	}

	// Search for one service
	public function getService()
	{
		$query = "select * from tb_service where idtb_service = ".$this->id;
		return mysqli_query($this->connect(), $query);
	}

	// Get specific services for cart
	public function getSpecificServices($services)
	{
		$query = "select * from tb_service where idtb_service in (";
		$counter = 0;
		foreach ($services as $id) {
			$counter++;
			$query .= $id;
			if($counter < count($services)) {
				$query .= ",";
			}
		}
		$query .= ")";
		return mysqli_query($this->connect(), $query);
	}

	// Delete service
	public function delete()
	{
		$query = "delete from tb_service where idtb_service = ".$this->id;
		return mysqli_query($this->connect(), $query);
	}

	// Edit a service
	public function edit()
	{
		$query = "update tb_service set name = '".$this->name."', price = '".$this->price."', description = '".$this->description."', details = '".$this->details."' where idtb_service = ".$this->id;
		return mysqli_query($this->connect(), $query);
	}

}

?>