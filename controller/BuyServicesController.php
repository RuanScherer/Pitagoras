<?php 

include_once($_SERVER['DOCUMENT_ROOT']."/Pitagoras/model/BuyServices.php");

class BuyServicesController extends BuyServices
{

	public function __construct($post)
	{
		parent::__construct($post);
	}

	public function insert()
	{
		$query = "insert into tb_buy_services(buy, service) values";
		foreach ($this->services as $service) {
			$query .= "('".$this->buy."', '".$service."'),";
		}
		$query = substr($query, 0, -1);
		$response = mysqli_query($this->connect(), $query);	

		if($response)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function getServiceNames()
	{
		$query = "
		select name
		from tb_buy_services bs
		inner join tb_service s
		on idtb_service = service
		where buy = ".$this->buy;
		return mysqli_query($this->connect(), $query);	
	}

}

?>