<?php

include_once($_SERVER['DOCUMENT_ROOT']."/Pitagoras/scripts/session-exists.php");
redirectIfLogged("admin", "admin/dashboard.php");

include_once("controller/UserController.php");

?>

<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="bootstrap/bootstrap.css">
    <link rel="stylesheet" href="styles.css">

    <link rel="sortcut icon" href="assets/logo.png" type="image/png"/>

    <!-- AOS Library -->
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

    <title>CFC Pitágoras</title>
  </head>
  <body>
    <div class="d-flex flex-column justify-content-center align-items-center bg-yellow w-100 mh-100vh px-4 py-5">
      <form method="post" action="?login=1" class="w-100 card b-radius shadow text-left d-flex flex-column align-items-start p-4 p-md-5" data-aos="fade-up" style="max-width: 650px;">
        <h2 class="font-weight-bold">Bem-vindo</h2>
        <h5 class="text-muted mb-4">Faça login para começar a administrar seu site.</h5>
        <div class="form-group w-100">
          <label for="login" class="font-weight-bold text-dark">Usuário</label>
          <input type="text" class="form-control" id="login" name="login" required>
        </div>
        <div class="form-group w-100">
          <label for="password" class="font-weight-bold text-dark">Senha</label>
          <input type="password" class="form-control" id="password" name="password" required>
        </div>
        <?php

        if(isset($_GET['login']))
        {
          $user = new UserController($_POST);
          if($user->get())
          {
            Header("Location: admin/dashboard.php");
          }
          else
          {
          
          ?>

            <p class='alert alert-danger alert-dismissible fade show w-100' role='alert'>
              Erro ao logar, tente novamente.
              <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button>
            </p>
          
          <?php

          }
        }

        ?>
        <button id="next" class="btn btn-dark align-self-end font-weight-bold mt-2">Entrar</button>
        <footer class="d-flex justify-content-center mt-4 w-100">
          <span class="font-weight-bold text-dark text-center">
            Desenvolvido por
            <a class="link text-decoration-none instagram-color" href="https://www.instagram.com/scherer_programmer/" target="_blank" rel="noopener">
              <strong>Ruan Scherer &#8599;</strong>
            </a>
          </span>
        </footer>
      </form> 
    </div>

    <script src="js/jquery.slim.min.js"></script>
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script type="text/javascript">
      AOS.init();
    </script>
  </body>
</html>