<?php

include_once("../../controller/MessageController.php");
include_once($_SERVER['DOCUMENT_ROOT']."/Pitagoras/scripts/logout.php");
include_once($_SERVER['DOCUMENT_ROOT']."/Pitagoras/scripts/session-doesnt-exist.php");
redirectIfNotLogged("admin", "../../login.php");

$messageController = new MessageController($_POST);

$messageController->autodestruct();
$messages = $messageController->getAll();
$messageController->setReaded();

?>

<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../../bootstrap/bootstrap.css">
    <link rel="stylesheet" href="../../styles.css">

    <title>CFC Pitágoras - Administrador</title>
  </head>
  <body>
  	<!-- HEADER -->
		<nav class="navbar px-4 py-3 navbar-light bg-yellow bl-radius br-radius shadow justify-content-center">
			<h5 class="navbar-brand">CFC Pitágoras - Administrador</h5>
		</nav>

		<section class="w-100 pt-4 px-4">
				<h1 class="font-weight-bold col-12">Mensagens</h1>
				<div class="d-flex flex-wrap">
					<div class="col-lg-4 d-flex flex-column my-2">
						<h4 class="text-dark mb-1">Acessos</h4>
						<div class="list-group w-100 mt-3 shadow-sm">
							<a href="../dashboard.php" class="list-group-item list-group-item-action font-weight-bold text-muted d-flex align-items-center">
								<svg class="bi bi-clipboard-data mr-2" width="1.5em" height="1.5em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
								  <path fill-rule="evenodd" d="M4 1.5H3a2 2 0 00-2 2V14a2 2 0 002 2h10a2 2 0 002-2V3.5a2 2 0 00-2-2h-1v1h1a1 1 0 011 1V14a1 1 0 01-1 1H3a1 1 0 01-1-1V3.5a1 1 0 011-1h1v-1z" clip-rule="evenodd"/>
								  <path fill-rule="evenodd" d="M9.5 1h-3a.5.5 0 00-.5.5v1a.5.5 0 00.5.5h3a.5.5 0 00.5-.5v-1a.5.5 0 00-.5-.5zm-3-1A1.5 1.5 0 005 1.5v1A1.5 1.5 0 006.5 4h3A1.5 1.5 0 0011 2.5v-1A1.5 1.5 0 009.5 0h-3z" clip-rule="evenodd"/>
								  <path d="M4 11a1 1 0 112 0v1a1 1 0 11-2 0v-1zm6-4a1 1 0 112 0v5a1 1 0 11-2 0V7zM7 9a1 1 0 012 0v3a1 1 0 11-2 0V9z"/>
								</svg>
								Dashboard
							</a>
						  <a href="overview.php" class="list-group-item list-group-item-action font-weight-bold text-muted d-flex align-items-center">
						  	<svg class="bi bi-envelope mr-2" width="1.5em" height="1.5em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
								  <path fill-rule="evenodd" d="M14 3H2a1 1 0 00-1 1v8a1 1 0 001 1h12a1 1 0 001-1V4a1 1 0 00-1-1zM2 2a2 2 0 00-2 2v8a2 2 0 002 2h12a2 2 0 002-2V4a2 2 0 00-2-2H2z" clip-rule="evenodd"/>
								  <path fill-rule="evenodd" d="M.071 4.243a.5.5 0 01.686-.172L8 8.417l7.243-4.346a.5.5 0 01.514.858L8 9.583.243 4.93a.5.5 0 01-.172-.686z" clip-rule="evenodd"/>
								  <path d="M6.752 8.932l.432-.252-.504-.864-.432.252.504.864zm-6 3.5l6-3.5-.504-.864-6 3.5.504.864zm8.496-3.5l-.432-.252.504-.864.432.252-.504.864zm6 3.5l-6-3.5.504-.864 6 3.5-.504.864z"/>
								</svg>
						  	Mensagens
						  </a>
						  <a href="../sales/overview.php" class="list-group-item list-group-item-action font-weight-bold text-muted d-flex align-items-center">
						  	<svg class="bi bi-briefcase mr-2" width="1.5em" height="1.5em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
								  <path fill-rule="evenodd" d="M0 12.5A1.5 1.5 0 001.5 14h13a1.5 1.5 0 001.5-1.5v-6h-1v6a.5.5 0 01-.5.5h-13a.5.5 0 01-.5-.5v-6H0v6z" clip-rule="evenodd"/>
								  <path fill-rule="evenodd" d="M0 4.5A1.5 1.5 0 011.5 3h13A1.5 1.5 0 0116 4.5v2.384l-7.614 2.03a1.5 1.5 0 01-.772 0L0 6.884V4.5zM1.5 4a.5.5 0 00-.5.5v1.616l6.871 1.832a.5.5 0 00.258 0L15 6.116V4.5a.5.5 0 00-.5-.5h-13zM5 2.5A1.5 1.5 0 016.5 1h3A1.5 1.5 0 0111 2.5V3h-1v-.5a.5.5 0 00-.5-.5h-3a.5.5 0 00-.5.5V3H5v-.5z" clip-rule="evenodd"/>
								</svg>
						  	Vendas
						  </a>
						  <a href="../newsletter-subscriptions/overview.php" class="list-group-item list-group-item-action font-weight-bold text-muted d-flex align-items-center">
						  	<svg class="bi bi-newspaper mr-2" width="1.5em" height="1.5em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
								  <path fill-rule="evenodd" d="M0 2A1.5 1.5 0 011.5.5h11A1.5 1.5 0 0114 2v12a1.5 1.5 0 01-1.5 1.5h-11A1.5 1.5 0 010 14V2zm1.5-.5A.5.5 0 001 2v12a.5.5 0 00.5.5h11a.5.5 0 00.5-.5V2a.5.5 0 00-.5-.5h-11z" clip-rule="evenodd"/>
								  <path fill-rule="evenodd" d="M15.5 3a.5.5 0 01.5.5V14a1.5 1.5 0 01-1.5 1.5h-3v-1h3a.5.5 0 00.5-.5V3.5a.5.5 0 01.5-.5z" clip-rule="evenodd"/>
								  <path d="M2 3h10v2H2V3zm0 3h4v3H2V6zm0 4h4v1H2v-1zm0 2h4v1H2v-1zm5-6h2v1H7V6zm3 0h2v1h-2V6zM7 8h2v1H7V8zm3 0h2v1h-2V8zm-3 2h2v1H7v-1zm3 0h2v1h-2v-1zm-3 2h2v1H7v-1zm3 0h2v1h-2v-1z"/>
								</svg>
						  	Inscrições na newsletter
						  </a>
						  <a href="../services/overview.php" class="list-group-item list-group-item-action font-weight-bold text-muted d-flex align-items-center">
						  	<svg class="bi bi-layout-text-sidebar-reverse mr-2" width="1.5em" height="1.5em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
								  <path fill-rule="evenodd" d="M2 1h12a1 1 0 011 1v12a1 1 0 01-1 1H2a1 1 0 01-1-1V2a1 1 0 011-1zm12-1a2 2 0 012 2v12a2 2 0 01-2 2H2a2 2 0 01-2-2V2a2 2 0 012-2h12z" clip-rule="evenodd"/>
								  <path fill-rule="evenodd" d="M5 15V1H4v14h1zm8-11.5a.5.5 0 00-.5-.5h-5a.5.5 0 000 1h5a.5.5 0 00.5-.5zm0 3a.5.5 0 00-.5-.5h-5a.5.5 0 000 1h5a.5.5 0 00.5-.5zm0 3a.5.5 0 00-.5-.5h-5a.5.5 0 000 1h5a.5.5 0 00.5-.5zm0 3a.5.5 0 00-.5-.5h-5a.5.5 0 000 1h5a.5.5 0 00.5-.5z" clip-rule="evenodd"/>
								</svg>
						  	Produtos / Serviços
						  </a>
						  <a href="../posts/overview.php" class="list-group-item list-group-item-action font-weight-bold text-muted d-flex align-items-center">
						  	<svg class="bi bi-file-post mr-2" width="1.5em" height="1.5em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
								  <path fill-rule="evenodd" d="M4 1h8a2 2 0 012 2v10a2 2 0 01-2 2H4a2 2 0 01-2-2V3a2 2 0 012-2zm0 1a1 1 0 00-1 1v10a1 1 0 001 1h8a1 1 0 001-1V3a1 1 0 00-1-1H4z" clip-rule="evenodd"/>
								  <path d="M4 5.5a.5.5 0 01.5-.5h7a.5.5 0 01.5.5v7a.5.5 0 01-.5.5h-7a.5.5 0 01-.5-.5v-7z"/>
								  <path fill-rule="evenodd" d="M4 3.5a.5.5 0 01.5-.5h5a.5.5 0 010 1h-5a.5.5 0 01-.5-.5z" clip-rule="evenodd"/>
								</svg>
						  	Posts
						  </a>
						</div>
						<h4 class="text-dark mb-1 mt-3">Ações</h4>
						<div class="list-group w-100 mt-3 shadow-sm">
						  <a href="../../index.php" target="_blank" class="list-group-item list-group-item-action font-weight-bold text-muted d-flex align-items-center">
						  	<svg class="bi bi-window mr-2" width="1.5em" height="1.5em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
								  <path fill-rule="evenodd" d="M14 2H2a1 1 0 00-1 1v10a1 1 0 001 1h12a1 1 0 001-1V3a1 1 0 00-1-1zM2 1a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V3a2 2 0 00-2-2H2z" clip-rule="evenodd"/>
								  <path fill-rule="evenodd" d="M15 6H1V5h14v1z" clip-rule="evenodd"/>
								  <path d="M3 3.5a.5.5 0 11-1 0 .5.5 0 011 0zm1.5 0a.5.5 0 11-1 0 .5.5 0 011 0zm1.5 0a.5.5 0 11-1 0 .5.5 0 011 0z"/>
								</svg>
						  	Ir para o site &#8599;
						  </a>
						  <a href="https://api.whatsapp.com/send?phone=5547999254336" target="_blank" rel="noopener" class="list-group-item list-group-item-action font-weight-bold text-muted d-flex align-items-center">
						  	<svg class="bi bi-code mr-2" width="1.5em" height="1.5em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
								  <path fill-rule="evenodd" d="M5.854 4.146a.5.5 0 010 .708L2.707 8l3.147 3.146a.5.5 0 01-.708.708l-3.5-3.5a.5.5 0 010-.708l3.5-3.5a.5.5 0 01.708 0zm4.292 0a.5.5 0 000 .708L13.293 8l-3.147 3.146a.5.5 0 00.708.708l3.5-3.5a.5.5 0 000-.708l-3.5-3.5a.5.5 0 00-.708 0z" clip-rule="evenodd"/>
								</svg>
						  	Contatar desenvolvedor &#8599;
						  </a>
						  <a href="?logout=1" class="list-group-item list-group-item-action font-weight-bold text-danger d-flex align-items-center">
						  	<svg class="bi bi-box-arrow-in-left mr-2" width="1.5em" height="1.5em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
								  <path fill-rule="evenodd" d="M7.854 11.354a.5.5 0 000-.708L5.207 8l2.647-2.646a.5.5 0 10-.708-.708l-3 3a.5.5 0 000 .708l3 3a.5.5 0 00.708 0z" clip-rule="evenodd"/>
								  <path fill-rule="evenodd" d="M15 8a.5.5 0 00-.5-.5h-9a.5.5 0 000 1h9A.5.5 0 0015 8z" clip-rule="evenodd"/>
								  <path fill-rule="evenodd" d="M2.5 14.5A1.5 1.5 0 011 13V3a1.5 1.5 0 011.5-1.5h8A1.5 1.5 0 0112 3v1.5a.5.5 0 01-1 0V3a.5.5 0 00-.5-.5h-8A.5.5 0 002 3v10a.5.5 0 00.5.5h8a.5.5 0 00.5-.5v-1.5a.5.5 0 011 0V13a1.5 1.5 0 01-1.5 1.5h-8z" clip-rule="evenodd"/>
								</svg>
						  	Encerrar sessão
						  </a>
						</div>
					</div>

					<aside class="col-lg-8 my-2">
						<h4 class="text-dark mb-1 text-left">Todas as mensagens</h4>
						<ul class="list-group w-100 list-unstyled mt-3 shadow-sm">
							<?php

							if(mysqli_num_rows($messages))
							{
								while($row = mysqli_fetch_assoc($messages))
								{
									echo "
									<a class='list-group-item list-group-item-action flex-column align-items-start";
									if ($row['READED'] == 0) {
										echo " bd-yellow";
									}
									echo "'>
								    <div class='d-flex w-100 justify-content-between'>
								      <h5 class='mb-1'>".$row['topic']."</h5>
								      <small>".date("d/m/Y", strtotime($row['date']))."</small>
								    </div>
								    <p class='mb-1'>".$row['message']."</p>
								    <small class='text-muted'>".$row['name']." (".$row['email'].")</small>
								  </a>
									";
								}
							}
							else
							{
							
							?>
								
								<a class='list-group-item list-group-item-action flex-column align-items-center px-3 py-5'>
							    <h4 class='text-muted mb-1 text-center op-08'>Parece que ainda não há nada por aqui.</h4>
							  </a>
							
							<?php

							}

							?>
						</ul>
					</aside>
				</div>
		</section>

		<footer class="d-flex justify-content-center py-4 px-4">
			<span class="font-weight-bold text-dark">
				Desenvolvido por
				<a class="link text-decoration-none instagram-color" href="https://www.instagram.com/scherer_programmer/" target="_blank" rel="noopener">
					<strong>Ruan Scherer &#8599;</strong>
				</a>
			</span>
		</footer>

    <script src="../../js/jquery.slim.min.js"></script>
    <script src="../../js/popper.js"></script>
    <script src="../../js/bootstrap.js"></script>
  </body>
</html>