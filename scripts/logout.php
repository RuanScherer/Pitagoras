<?php
session_start();

function searchUri($test) {
	if (strpos($_SERVER['REQUEST_URI'], $test) === false) {
		return false;
	}
	return true;
}

function logout($redirectTo) {
	session_destroy();
	header("Location: ".$redirectTo);
}

if(isset($_GET['logout'])) {
	if (searchUri("view/")) {
		logout("../index.php");
	}
	elseif (searchUri("admin")) {
		if (searchUri("dashboard.php")) {
			logout("../login.php");
		} else {
			logout("../../login.php");
		}
	} 
	else {
		logout("index.php");
	}
}

?>