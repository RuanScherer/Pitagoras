<?php

function redirectIfNotLogged($param, $url) {
	if (!isset($_SESSION[$param])) {
		header("Location: ".$url);
	}
}

?>