<?php
session_start();

function redirectIfLogged($param, $url) {
	if(isset($_SESSION[$param]))
	{
	  Header("Location: ".$url);
	}
	else
	{
	  session_destroy();
	}
}

?>